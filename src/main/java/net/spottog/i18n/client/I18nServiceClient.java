package net.spottog.i18n.client;

import java.util.Locale;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import net.spottog.i18n.TextSave.SearchReturnSave;
import net.spottog.i18n.global.ITextSave;
import net.spottog.i18n.global.ITextWithLanguage;
import net.spottog.i18n.global.ITextWithLanguageAnswer;
import net.spottog.i18n.global.TextWithLanguage;
import net.spottog.i18n.global.TextWithLanguageAnswer;
import net.spottog.i18n.global.TextWithLanguageRequest;

/**
 * Reicht die Anfragen an den I18n Service Server weiter.
 * Arbeitest als rest api client mit hilfe von @see javax.ws.rs.client.Client
 */
public class I18nServiceClient extends ITextSave
{
	private final String URL;
	private final String ApiKey;
	private final Client client;
	/**
	 * Initialisiert einen Webclient, welcher einen I18nService befragt.
	 * @param URL Die Adresse, unter welcher der I18nService zu erreichen ist (nicht der rest endpunkt).
	 * @param ApiKey Der ApiKey. Einzustellem im Service.
	 */
	public I18nServiceClient(final String URL, final String ApiKey) {
		this(URL, ApiKey, null);
	}
	/**
	 * Initialisiert einen Webclient, welcher einen I18nService befragt.
	 * @param URL URL Die Adresse, unter welcher der I18nService zu erreichen ist (nicht der rest endpunkt).
	 * @param ApiKey ApiKey Der ApiKey. Einzustellem im Service.
	 * @param searchReturnSave Wird ein SearchReturnSave mitgegeben, wird bei fehlgeschlagenen Suchen, das Suchwort zurück gegeben.
	 */
	public I18nServiceClient(final String URL, final String ApiKey,final ITextSave searchReturnSave) {
		super(searchReturnSave);
		this.URL = URL + "/rest/text";
		this.ApiKey = ApiKey;
		client = ClientBuilder.newClient();
	}

	@Override
	public ITextWithLanguage GetLocal(String search, Locale Language) {
		Response response = client.target(URL).request(MediaType.APPLICATION_JSON).post(Entity.entity(new TextWithLanguageRequest(ApiKey, search, Language), MediaType.APPLICATION_JSON));
		if(response.getStatus() == 204) return null; //nicht gefunden
		if(response.getStatus() != 200)
			throw new IllegalAccessError(response.toString());
		ITextWithLanguageAnswer anser = response.readEntity(TextWithLanguageAnswer.class);
		if(anser == null) return null;
		return new TextWithLanguage(anser);
	}
	/**
	 * Gibt den Api Key Zurück.
	 * @return den key als String
	 */
	public String getApiKey() {
		return ApiKey;
	}
	/**
	 * Gibt die URL zurück.
	 * @return die komplette Url zum rest Service als String.
	 */
	public String getURL() {
		return URL;
	}

}
