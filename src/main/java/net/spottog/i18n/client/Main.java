package net.spottog.i18n.client;

import java.util.Locale;

import net.spottog.i18n.TextSave.FakeSave;
import net.spottog.i18n.TextSave.InMemorySave;
import net.spottog.i18n.global.I18n;
import net.spottog.i18n.global.ITextWithLanguage;
/**
 * Test bzw. Beispiel wie der Client benutzt werden kann.
 * 
 *
 */
public class Main {
	/**
	 * Start Methode
	 * @param args nicht benutzt.
	 */
	private static final String SERVER = "i18n";
	//private static final String SERVER = "127.0.0.1";
	private static final String APITOKEN = "ZMAdxJpjLlxB4KRAmlGuxdXfX3XPi90tBCuJSLt7LDYDHMSZMjLeWDACMLqJuD-4";
	private static final String URL = "http://"+SERVER+":8080/i18n-service";
	public static void main(String[] args) {
		I18n.Save = new InMemorySave(new I18nServiceClient(URL, APITOKEN, new FakeSave(null, true)));
		System.out.println("Suche das wort: New");
		System.out.println(new I18n("new"));
		
		
		
		//Test Error
		System.out.println("Suche das wort: Fehler");
		System.out.println(new I18n("Fehler"));
	}

}
