package net.spottog.i18n.client;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.Test;

import net.spottog.i18n.global.ITextWithLanguage;
/**
 * Test für den CLient.
 */
class ClientBenchmark {
	/**
	 * Startet mehrere Aufrufe mit dem client in einem Einzelnen thread gegen den Service und misst die zeit.
	 */
	@Test
	void singleTest() {
		I18nServiceClient client = new I18nServiceClient("http://localhost:8080/i18n-service", "fwCJEpBj2Kol_YuJc1MsyroA-Ux6Gxr3aT3Mg4EfjgWdzHla2sh798NXSoYPBLZc");
		long starttime = (new Date()).getTime();
		int runs = 100000;
		for(int i = 0; i < runs; i++) {			
			ITextWithLanguage text = client.Get("test", new Locale("de"));
			assertTrue(text.isValid());
		}
		long time = (new Date()).getTime() - starttime;
		System.out.println(runs + " runs in " + time + "ms (Singlethreading) avg: " + ((double)time/(double)runs));
	}
	/**

	 * Startet mehrere Aufrufe mit dem client in mehrerenden thread gegen den Service und misst die zeit.
	 * @throws InterruptedException wegen join um zu warten bis alle thread abgeschlossen sind.
	 */
	@Test
	void MultiTest() throws InterruptedException {
		final I18nServiceClient client = new I18nServiceClient("http://localhost:8080/i18n-service", "fwCJEpBj2Kol_YuJc1MsyroA-Ux6Gxr3aT3Mg4EfjgWdzHla2sh798NXSoYPBLZc");
		long starttime = (new Date()).getTime();
		final int cmpltruns = 200000;
		final AtomicInteger runs = new AtomicInteger(cmpltruns);
		Thread[] threads = new Thread[1000];
		for(int i = 0; i<threads.length; i++) {
			threads[i] = new Thread(new Runnable() {
				public void run() {
					while (runs.decrementAndGet() > 0) {
						ITextWithLanguage text = client.Get("test", new Locale("de"));
						assertTrue(text.isValid());
					}
				}
			});
			threads[i].start();
		}
		for(int i = 0; i<threads.length; i++) {
			threads[i].join();
		}
		long time = (new Date()).getTime() - starttime;
		System.out.println(cmpltruns + " runs in " + time + "ms (" +threads.length +" Threads) avg: " + ((double)time/(double)cmpltruns));
	}

}

